-- a. Find all artists that have letter d in its name.
    -- SELECT * FROM songs WHERE song_name LIKE "%a%";
    SELECT * FROM artists WHERE name LIKE "%d%";

-- b. Find all songs that have a length of less than 230.
    -- SELECT * FROM songs WHERE id < 11;
    SELECT * FROM songs WHERE length < "00:03:30";

-- c. Join the "albums" and "songs" tables. (Only show the album name, song name, and song length)
    -- SELECT artists.name, albums.album_title FROM artists 
        -- JOIN albums ON artists.id = albums.artist_id;
    -- SELECT * FROM artists
        -- JOIN albums ON artists.id = albums.artist_id
        -- JOIN songs ON albums.id = songs.album_id;

    -- SELECT albums.album_title, songs.song_name, length FROM artists
    --     JOIN albums ON artists.id = albums.artist_id
    --     JOIN songs ON albums.id = songs.album_id;
    SELECT albums.album_title, songs.song_name, length from albums
        JOIN songs ON albums.id = songs.album_id;

-- d. Join the "artists" and "albums" tables. (Find all albums that has a letter 'a' in its name.)
    SELECT * FROM artists
        JOIN albums ON artists.id = albums.artist_id
        WHERE albums.album_title LIKE "%a%";

-- e. Sort the albumns in Z-A order (Show only the first 4 records.)
    SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- f. Join the 'albums' and 'songs' tables. (Sort albums from Z-A)
    SELECT * FROM albums
        JOIN songs ON albums.id = songs.album_id
        ORDER BY album_title DESC;